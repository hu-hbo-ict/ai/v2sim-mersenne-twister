%\documentclass{article}
\documentclass[a4paper]{tufte-handout-peikos}

\usepackage{amsmath,amssymb,amsthm,amsfonts}
\usepackage[normalem]{ulem}
\usepackage{calligra}
\usepackage{csquotes,multicol,hyperref}
\usepackage{natbib}
\usepackage[dutch]{babel}
\hypersetup{colorlinks=true, citecolor=RubineRed, urlcolor=Cerulean, linkcolor=Cerulean}

\title{De Mersenne Twister}
\author{Brian van der Bijl}

\def\vec#1{\mathbf{#1}}
\def\choice#1{{\color{BurntOrange} #1}}

\begin{document}
\maketitle

De Mersenne Twister\cite{matsumotoMersenneTwister623dimensionally1998} is een veelgebruikte pseudo-random number generator met goede statistische eigenschappen\cite{perrosComputerSimulationTechniques2009}. De standaard-implementatie (MT19937) genereert random numbers van 32-bits en heeft als periode het 24e Mersenne priemgetal, $2^{19937}-1$. Het algoritme levert positieve integers op, die daarna geschaald\sidenote{De 32-bits waardes kunnen eventueel ook direct als andere datatypes beschouwd worden, bijvoorbeeld voor random characters. Voor floats werkt dit niet direct, omdat de beschikbare floats niet netjes over de getallenlijn verdeeld zijn en we derhalve geen uniforme verdeling behouden. Hier is het dus verstandig de integers door $2^{32}-1$ te delen.} kunnen worden. De Mersenne Twister gebruikt een redelijk aantal parameters, waarvan de waarde de effectiviteit van het algoritme kunnen be\"{i}nvloeden. \choice{Zinnen in dit document in het oranje beschrijven de bewezen keuzes die we voor nu zullen hanteren, maar die strict genomen niet deel van het algoritme zijn.}

\section{Werking}
De Mersenne Twister gebruikt zogenaamde \emph{word vectors\footnote{Een word duidt hier op een hoeveelheid bits, typisch de hoeveelheid bits die een computer parallel kan verwerken. Voor moderne systemen is dit 64-bits, bij de Mersenne Twister wordt vaak nog 32-bits gebruikt. Het heeft dus niet met taalkundige woorden / word vectors te maken.}}, waarmee $w$-dimensionale row-vectors over het lichaam\sidenote{Een lichaam (Engels: \emph{\href{https://en.wikipedia.org/wiki/Field_(mathematics)}{Field}}) is een algebraische structuur die onder andere $\mathbb{R}$ abstraheert.} $\{0, 1\}$ (ook bekend als $\mathbb{B}$ of $\mathbb{F}_{2}$) bedoeld worden. Normaliter kan iedere plek in een vector alle getallen in $\mathbb{R}$ bevatten (althans, de vectoren die wij zover gezien hebben), maar nu wordt dit beperkt tot enkel de bitwaardes $1$ en $0$.

Het algoritme is gebaseerd op de lineaire recurrente betrekking:

$$\vec x_{k+n} \leftarrow \vec x_{k+m} \oplus (\vec x^{u}_{k}\ |\ \vec x^{l}_{k+1} )A \quad (k \in \mathbb{N}_{0})$$

Hier staat $\oplus$ voor de XOR operatie en $|$ voor concatenatie. Superscripten $u$ en $l$ worden hier gebruikt om respectievelijk de \emph{upper} en \emph{lower} bits te selecteren. $x^{u}_{k}$ geeft de bovenste $w-r$ bits van $x_{k}$, $x^{l}_{k}$ de onderste $r$. $w$ is de dimensie van de vectoren: $32$ bits. \choice{Voor $r$ kiezen we $31$, dus we nemen de upper (eerste) $1$ bit en de lower (laatste) $31$ bits.} $n$ staat voor de graad van de recurrente betrekking en geeft aan hoeveel pseudo-random getallen we per batch klaarzetten. $m$ is een getal tussen $1$ en $n$. \choice{Zowel $n$ als $m$ zijn voor ons gekozen: $n=624$ en $m=397$.}

$A$ is een $w\times w$ matrix (ook hier met coefficienten in $\{0, 1\}$) die hieronder gegeven staat. De matrix is zo gekozen dat deze met een right bit shift \footnote{\texttt{>>} in Python} en XOR \footnote{\texttt{\^} in Python} snel uit te voeren is op de computer.

$$A = \begin{bmatrix}
  & 1 & & & \\
  & & 1 & & \\
  & & & \ddots & \\
  & & & & 1 \\
  a_{w-1} & a_{w-2} & \cdots & \cdots & a_{0}
\end{bmatrix}$$

Voor alle bits behalve de laatste is de bit $1$ plaats opgeschoven (de bit shift), en bij de laatste bit wordt na de shift de vector $a$ \choice{(in ons geval de bit vector corresponderend met het hexadecimale getal \texttt{0x9908b0df})} met behulp van XOR binair opgeteld:

\marginnote{Let op: $x_0$ is de least significant bit, oftewel de meest rechter bit. Deze bit geeft aan of een getal even of oneven is.}

$$\vec xA = \begin{cases}
\text{rightshift}(\vec x) & \text{ als } x_{0} = 0 \\
\text{rightshift}(\vec x) \oplus \vec a & \text{ als } x_{0} = 1 \\
\end{cases}
$$\marginnote{Let op: de vector $\vec a$ is, net als alle andere vectoren in deze code, een row-vector. Om deze reden staat bij de matrix-vermenigvuldiging de matrix ook aan de rechterkant.}

Ieder gegenereerde $32$-bits \emph{word} wordt (om de statistische eigenschappen te verbeteren) voordat deze wordt opgeleverd nog vermenigvuldigd met een omkeerbare \enquote{temperance} matrix $T$, die ook met behulp van bitwise operaties op te schrijven is:
\begin{align*}
y & \leftarrow x \oplus (x >> u) \\
y & \leftarrow y \oplus ((y << s) \land b) \\
y & \leftarrow y \oplus ((y << t) \land c) \\
z & \leftarrow y \oplus (y >> l)
\end{align*}

De operator \texttt{<<} is de left shift, waarbij de bits naar links worden opgeschoven. De right shift en logische \emph{and} kennen we al. Bij beide shift operatoren geeft de tweede waarde aan hoeveel plaatsen de bits worden opgeschoven. De vectoren $b$ en $c$ zijn wederom parameters. \choice{In onze versie kiezen we de waardes $b =$ \texttt{0x9d2c5680} en $c =$ \texttt{0xefc60000}.} De waardes $l$, $s$, $t$ en $u$\footnote{Dit zijn een andere $u$ en $l$ dan de superscript die de upper/lower bits helemaal aan het begin aangaf.} zijn wederom vooraf vastgestelde parameters: \choice{$l = 18, s = 7, t = 15$ en $u = 11$.}

\section{Doorlopen van het algoritme}
In deze sectie wordt het algoritme in pseudo-code van start tot finish doorgelopen. We beginnen met het klaarzetten van de bitmaskers en het vullen van een initi\"{e}le seed. Hierna komen we in het herhalende deel van het algoritme.

\subsection{Klaarzetten van de bitmaskers}
In het algoritme gebruiken we bitmaskers om de upper/most significant en lower/least significant bits te selecteren. Een bitmasker is een reeks bits, waarbinnen een $1$ een bit selecteert en een $0$ een bit weggooit. Het totale bitmask wordt parallel met een bitvector ge-AND, waarbij de bits die in het mask op $1$ staan enkel nog afhangen van de bits in de bitvector ($\phi \land \top = \phi$). Bits die in het mask op $0$ staan komen altijd op $0$ uit, omdat $\phi \land \bot = \bot$. Deze bitmaskers noemen we $u$ voor upper en $ll$ voor lower\footnote{Waarom dit niet $l$ is, is beyond me. De variabelenaam $l$ wordt al in de stap met de temperance matrix gebruikt, maar als dat de reden was zou $u$ hier ook $uu$ zijn. Either way komt dit zo uit de originele paper.}. Daarnaast stellen we de onderste rij van de matrix $A$ vast, zodat hier mee ge-XOR-d kan worden:

\begin{align*}
\vec u & \leftarrow \underbrace{1\dots 1}_{w-r}\underbrace{0\dots 0}_{r} \\
\vec{ll} & \leftarrow \underbrace{0\dots 0}_{w-r}\underbrace{1\dots 1}_{r} \\
\vec a & \leftarrow a_{w-1} a_{w-2} \dots a_{1} a_{0} \\
\end{align*}

\subsection{Initialisatie van de seed}
De Mersenne Twister maakt gebruikt van een compleet block aan waardes dat als seed gebruikt wordt. Voor de eindgebruiker willen we echter dat de seed met een enkel getal instelbaar is. De rest zullen we dus moeten vullen met waardes die niet allemaal $0$ zijn. Hiertoe zetten we de seed als eerste getal in een array, en gebruiken een formule om de rest in te vullen. De seed moet door de gebruiker instelbaar zijn. \choice{Wij gebruiken de formule hieronder, een variatie van de Linear Congruential Generator, om het seed block te vullen:}
\choice{$$x_i = f \cdot \big(x_{i-1} \oplus \left(x_{i-1} >> \left(w-2\right)\right)\big) + i$$}
\choice{Hierbij is $f$ wederom een parameter: \texttt{0x6c078965}. De $\cdot$ is de gewone vermenigvuldiging, $+$ normaal optellen. Verder is het belangrijk de waarde binnen de 32-bits te houden, dit kan door te AND-en met de bitmask \texttt{0xffffffff}.}

\subsection{De twist}
Dit is de stap die voor ieder block getallen opnieuw herhaald gaat worden, waarbij $(\vec x^{u}_{k}\ |\ \vec x^{l}_{k+1})$ berekend wordt. Let op de modulo bij het ophogen van de indices\footnote{Dit is nodig omdat we steeds in blocks van formaat $n$ waardes genereren. Zodra we aan een nieuw block beginnen zouden we boven de gereserveerde hoeveelheid ruimte uitkomen, terwijl we deze willen hergebruiken.}. We gebruiken de variabele $y$ om onze (tussentijdse) waardes op te slaan. Na afloop wordt $y$ opgeslagen en herhaalt het proces zich met de volgende index. Let op dat we per twist 624 nieuwe getallen genereren, en daarbij de oude overschrijven. De nieuwe waardes worden een voor een berekend uit de waardes die op dat moment in de generator-state zitten, dus waar verwezen wordt naar $x[i]$ kan dit zowel uit het vorige block komen (als de nieuwe waarde nog niet berekend is) of uit het nieuwe (als de waarde eerder in dezelfde twist veranderd is). Let op dat je na het vullen van je seed-block een enkele twist uitvoert.
$$\vec y \leftarrow (\vec x [i] \land u) \lor (\vec x[(i+1) \text{ mod } n] \land ll ) $$

\begin{fullwidth}
Vervolgens de vermenigvuldiging met $A$, via bitwise operaties:
$$\vec x[i] \leftarrow \vec x[ (i+m) \text{ mod } n] \oplus (y >> 1) \oplus \begin{cases}
  0 & \text{als de least significant bit van $\vec y = 0$ } \\
  a & \text{als de least significant bit van $\vec y = 1$ }
  \end{cases}$$
\end{fullwidth}

Op deze manier wordt iedere keer een block van \choice{624} getallen gegenereerd dat als state in de RNG wordt opgeslagen. Zodra alle getallen opgeleverd zijn wordt de volgende batch gegenereerd.

\subsection{Opleveren van random getallen.}
Voor het opleveren van een random getal pakken we het eerst volgende nog niet opgeleverde getal in de state. Deze vermenigvuldigen we met matrix $T$, wederom uitgedrukt in bitwise operaties:
\begin{align*}
\vec y & \leftarrow \vec x[i] \\
\vec y & \leftarrow \vec y \oplus (\vec y >> u) \\
\vec y & \leftarrow \vec y \oplus ((\vec y << s) \land \vec b) \\
\vec y & \leftarrow \vec y \oplus ((\vec y << t) \land \vec c) \\
\vec y & \leftarrow \vec y \oplus (\vec y >> l) \quad\quad\quad\quad\quad\text{(Return waarde $y$)}
\end{align*}

En tot slot hogen we een interne index $i$ met $1$ op modulo $n$ zodat we de volgende keer een nieuw getal teruggeven. Zodra $i$ terug naar $0$ gaat volgt een nieuwe twist.

$$i \leftarrow (i + 1) \text{ mod } n$$

%f =
%seed = 5489

\begin{fullwidth}
\choice{
\section{Testen}
Als je alle parameter-waardes gekozen hebt zoals in dit document beschreven, kun je de implementatie van je algoritme testen door jouw output te vergelijken met de onderstaande getallen ($seed = 5489$):
\begin{multicols}{4}
3499211612 \\ 581869302\\ 3890346734\\ 3586334585\\ 545404204\\ 4161255391\\ 3922919429\\ 949333985\\ 2715962298\\ 1323567403\\ 418932835\\ 2350294565\\ 1196140740\\ 809094426\\ 2348838239\\ 4264392720\\ 4112460519\\ 4279768804\\ 4144164697\\ 4156218106\\ 676943009\\ 3117454609\\ 4168664243\\ 4213834039\\ 4111000746\\ 471852626\\ 2084672536\\ 3427838553\\ 3437178460\\ 1275731771\\ 609397212\\ 20544909\\ 1811450929\\ 483031418\\ 3933054126\\ 2747762695\\ 3402504553\\ 3772830893\\ 4120988587\\ 2163214728\\ 2816384844\\ 3427077306\\ 153380495\\ 1551745920\\ 3646982597\\ 910208076\\ 4011470445\\ 2926416934\\ 2915145307\\ 1712568902\\ 3254469058\\ 3181055693\\ 3191729660\\ 2039073006\\ 1684602222\\ 1812852786\\ 2815256116\\ 746745227\\ 735241234\\ 1296707006\\ 3032444839\\ 3424291161\\ 136721026\\ 1359573808
\end{multicols}
Het aanvankelijke seed-block (voor de eerste twist) zou daarbij met $5489$, $1301868182$, $2938499221$, $2950281878$, $1875628136$, $751856242$ moeten beginnen.
}

\addcontentsline{toc}{chapter}{Bibliography}
\bibliography{literature}
\bibliographystyle{plainnat}
\end{fullwidth}


\end{document}
