# Mersenne Twister

Dit is de broncode van het document "Mersenne Twister" dat dient ter aanvulling en vervanging van Perros voor les 10 van Simulation (Mathematics) van HBO-ICT AI jaar 2. Suggesties voor aanpassingen zijn welkom via PR. De meest recente versie van het document wordt bij iedere push gegenereerd, en is beschikbaar via [deze link](https://hu-hbo-ict.gitlab.io/ai/v2sim-mersenne-twister/MersenneTwister.pdf). Omdat het updaten van bestanden op Canvas handmatig gaat, kan het zijn dat deze versie meer up to date is.
