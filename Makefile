all: main.pdf
	mkdir -p public
	cp main.pdf public/MersenneTwister.pdf

main.pdf: main.tex literature.bib
	pdflatex main.tex
	bibtex main
	pdflatex main.tex
	pdflatex main.tex
